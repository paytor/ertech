<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Company extends Model
{
    use Notifiable;

    protected $fillable = [
        'name','email','logo','website','created_at','updated_at'
    ];

    public function routeNotificationForMail($notification)
    {
        return $this->email;
    }
}
