<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Employee;
use App\Model\Company;
use App\Notifications\EmployeeNotification;

use DataTables;
use Validator;
use Notification;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     
    public function json()
    {
        return Datatables::of(Employee::all())
        ->addColumn('action', function($row){
       
            $btn = '<a href="javascript:void(0)" onclick="editData('.$row->id.')" data-toggle="modal" data-target="#edit-modal" class="btn btn-info btn-sm mr-1"><i class="far fa-edit"></i> Edit</a>';
            $btn = $btn.'<a href="javascript:void(0)" onclick="deleteData('.$row->id.')" class="btn btn-danger btn-sm mr-1"><i class="far fa-trash-alt"></i> Delete</a>';

            return $btn;
        })
        ->addColumn('company_detail', function($row){
            $company = Company::where('id',$row->company)->first();
            
            if(isset($company)){
                $companyName = $company->name;                
                return "<button href='javascript:void(0)' onclick='companyDetail(".$company->id.")' data-toggle='modal' data-target='#company-modal' class='btn btn-link btn-sm'>".$companyName."</button>";
            }else{
                return '';
            }

        })
        ->addColumn('fullname', function($row){
            return $row->getFullNameAttribute();
        })
        ->rawColumns(['action', 'company_detail'])
        ->addIndexColumn()->make(true);
    }

    public function index()
    {
        $company = Company::get();
        return view('pages.employee.index', compact('company'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
        ]);

        if($validator->passes()){

            $data = Employee::create($input);

            $check_company = Company::where('id',$input['company'])->first();

            if(isset($check_company->email)){
                $details = [
                    'greeting' => 'New Employee',
                    'body' => 'Your company has new employe '.$data->getFullNameAttribute(),
                    'email' => $check_company->email
                ];
          
                Notification::send($check_company, new EmployeeNotification($details));
            }
            
            $response = [
                'error' => 0,
                'massage' => 'Success',
                'data' => $data
            ];

        }else{
            $massage = '';
            foreach($validator->errors()->all() as $val){
                $massage .= $val.' ';
            }
            $response = [
                'error' => 1,
                'massage' => $massage,
                'data' => ''
            ];
        }

        return response()->json($response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Employee::where('id',$id)->first();
        
        $company = Company::get();
        
        return view('pages.employee.edit', compact('data','company'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->except('_method');

        $validator = Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
        ]);

        $check = Employee::where('id',$id)->first();

        if($validator->passes()){
            if(isset($check)){
                $data = Employee::where('id',$id)->update($input);
                
                $response = [
                    'error' => 0,
                    'massage' => 'Success',
                    'data' => $data
                ];
            }else{
                $response = [
                    'error' => 1,
                    'massage' => 'Data not found',
                    'data' => ''
                ];
            }
        }else{
            $massage = '';
            foreach($validator->errors()->all() as $val){
                $massage .= $val.' ';
            }
            $response = [
                'error' => 1,
                'massage' => $massage,
                'data' => ''
            ];
        }

        return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = Employee::where('id',$id)->delete();

        if($delete){
            $response = [
                'error' => 0,
                'massage' => 'Success delete',
                'data' => ''
            ];
        }else{
            $response = [
                'error' => 1,
                'massage' => 'Something wrong',
                'data' => ''
            ];
        }

        return response()->json($response);
    }

}
