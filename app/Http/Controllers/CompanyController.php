<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Company;
use App\User;

use Validator;
use DataTables;
use File;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function json()
    {
        return Datatables::of(Company::all())
        ->addColumn('action', function($row){
       
            $btn = '<button type="button" class="btn btn-sm btn-info mr-1" onclick="editData('.$row->id.')" data-toggle="modal" data-target="#edit-modal"><i class="far fa-edit"></i> Edit</button>';
            $btn = $btn.'<a href="javascript:void(0)" onclick="deleteData('.$row->id.')" class="btn btn-danger btn-sm mr-1"><i class="far fa-trash-alt"></i> Delete</a>';

            return $btn;
        })
        ->addColumn('logo_url', function($row){
        
            if(isset($row->logo)){
                $img = '<img src="/storage/'.$row->logo.'" class="img-thumbnail" alt="'.$row->name.'">';
            }else{
                $img = '';
            }

            return $img;
        })
        ->addColumn('website_url', function($row){
        
            if(isset($row->website)){
                if(str_contains($row->website, 'https://') || str_contains($row->website, 'http://')){
                    $website = '<a href="'.$row->website.'" class="btn btn-link" target="_blank">'.$row->website.'</a>';
                }else{
                    $website = '<a href="https://'.$row->website.'" class="btn btn-link" target="_blank">https://'.$row->website.'</a>';
                }
                
            }else{
                $website = '';
            }

            return $website;
        })
        ->rawColumns(['action', 'logo_url','website_url'])
        ->addIndexColumn()->make(true);
    }

    public function index()
    {
        return view('pages.company.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);

        if($validator->passes()){
            if(isset(request()->logo)){
                $imageName = time().'.'.request()->logo->getClientOriginalExtension();
                $input['logo'] = $imageName;
                request()->logo->move(storage_path('app/public'), $imageName);
            }else{
                $input['logo'] = null;
            }

            $data = Company::create($input);
            
            $response = [
                'error' => 0,
                'massage' => 'Success',
                'data' => $data
            ];

        }else{
            $massage = '';
            foreach($validator->errors()->all() as $val){
                $massage .= $val.' ';
            }
            $response = [
                'error' => 1,
                'massage' => $massage,
                'data' => ''
            ];
        }

        return response()->json($response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Company::where('id',$id)->first();
        
        return view('pages.employee.company', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Company::where('id',$id)->first();
        
        return view('pages.company.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = $request->except('_method');

        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);

        $check = Company::where('id',$id)->first();

        if($validator->passes()){
            if(isset($check)){
                if(isset(request()->logo)){
                    
                    $imageName = time().'.'.request()->logo->getClientOriginalExtension();
                    $input['logo'] = $imageName;
                    request()->logo->move(storage_path('app/public'), $imageName);
                    
                    if(isset($check->logo)){
                        if(File::exists(storage_path('app/public/'.$check->logo))) {
                            File::delete(storage_path('app/public/'.$check->logo));
                        }
                    }
                }else{
                    unset($input['logo']);
                }

                $data = Company::where('id',$id)->update($input);
                
                $response = [
                    'error' => 0,
                    'massage' => 'Success',
                    'data' => $data
                ];
            }else{
                $response = [
                    'error' => 1,
                    'massage' => 'Data not found',
                    'data' => ''
                ];
            }
        }else{
            $massage = '';
            foreach($validator->errors()->all() as $val){
                $massage .= $val.' ';
            }
            $response = [
                'error' => 1,
                'massage' => $massage,
                'data' => ''
            ];
        }

        return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $check = Company::where('id',$id)->first();

        if(isset($check->logo)){
            if(File::exists(storage_path('app/public/'.$check->logo))) {
                File::delete(storage_path('app/public/'.$check->logo));
            }
        }

        $delete = Company::where('id',$id)->delete();

        if($delete){
            $response = [
                'error' => 0,
                'massage' => 'Success delete',
                'data' => ''
            ];
        }else{
            $response = [
                'error' => 1,
                'massage' => 'Something wrong',
                'data' => ''
            ];
        }

        return response()->json($response);
    }
}
