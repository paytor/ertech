<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['register' => false]);

Route::middleware(['auth', 'admin'])->prefix('admin')->group(function () {
    Route::get('company/json', 'CompanyController@json');
    Route::resource('company', 'CompanyController');

    Route::get('employee/json', 'EmployeeController@json');
    Route::resource('employee', 'EmployeeController');

    Route::get('send', 'NotifyController@index');
    Route::get('mail', 'EmployeeController@mail');

});

Route::get('/home', function() {
    return view('home');
})->name('home')->middleware('auth');
