@extends('adminlte::page')

@section('title', 'GR Tech')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
@stop

@section('content_header')
    <h1 class="m-0 text-dark">Company</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#create-modal">
                        <i class='fas fa-plus-circle'></i> Create
                    </button>
                    <br/><br/>
                    <div class="table-responsive">
                    <table class="table table-bordered" id="company" width="100%">
                        <thead>
                            <tr>
                                <th>Index</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Logo</th>
                                <th>Website</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- The Create Modal -->
    <div class="modal fade" id="create-modal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
            <form enctype="multipart/form-data" id="create-form">
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Create</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                
                <!-- Modal body -->
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="name">Name:</label>
                                <input type="text" class="form-control" id="name" name="name">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="email">Email:</label>
                                <input type="email" class="form-control" id="email" name="email">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="email">Logo:</label>
                                <!-- <input type="file" name="logo" id="logo" accept="image/png, image/gif, image/jpeg" class="form-control"> -->
                                <div class="custom-file mb-3">
                                    <input type="file" class="custom-file-input" id="logo" accept="image/png, image/gif, image/jpeg" name="logo">
                                    <label class="custom-file-label" for="logo">Choose file</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="website">Website:</label>
                                <input type="text" class="form-control" id="website" name="website">
                            </div>
                        </div>
                    </div>
                    
                </div>
                
                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="submit" id="create-submit" class="btn btn-success mr-1"><i class='fas fa-plus-circle'></i> Submit</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class='far fa-window-close'></i> Close</button>
                </div>
                
            </form>
            </div>
        </div>
    </div>

    <!-- The Edit Modal -->
    <div class="modal fade" id="edit-modal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
            <form enctype="multipart/form-data" id="edit-form">
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Edit</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                
                <!-- Modal body -->
                <div class="modal-body">
                    <div id="edit-loader">
                        <center><i class="fa fa-spinner fa-spin" style="font-size:24px"></i></center>
                    </div>
                    <div id="edit-content"></div>
                </div>
                
                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="submit" style="display:none;" id="edit-submit" class="btn btn-info mr-1"><i class='far fa-edit'></i> Edit</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class='far fa-window-close'></i> Close</button>
                </div>
                
            </form>
            </div>
        </div>
    </div>
    
@stop

@section('js')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
    <script>
        $( document ).ready(function() {

            getData()

            $('#create-form').on('submit',(function(e) {
                $.ajaxSetup({
                    headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                })

                $("#create-submit").attr("disabled", true)

                e.preventDefault();
                var formData = new FormData(this);

                $.ajax({
                type:'POST',
                url: "/admin/company",
                data:formData,
                cache:false,
                contentType: false,
                processData: false,
                
                    complete: function(response) 
                    {
                        $("#create-submit").attr("disabled", false)

                        if(response.responseJSON.error == 0){

                            $("#name").val('')
                            $("#email").val('')
                            $("#logo").val('')
                            $("#website").val('')

                            $('#create-modal').click();

                            Swal.fire({
                                icon: 'success',
                                title: 'Success',
                                text: response.responseJSON.massage,
                            })
                            
                            $('#company').DataTable().ajax.reload()

                        }else{
                            Swal.fire({
                                icon: 'error',
                                title: 'Error',
                                text: response.responseJSON.massage,
                            })
                        }
                    }

                })
            }))

            $('#edit-form').on('submit',(function(e) {
                $.ajaxSetup({
                    headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                })

                $("#edit-submit").attr("disabled", true)

                e.preventDefault()
                var formData = new FormData(this)

                $.ajax({
                type:'POST',
                url: "/admin/company/"+formData.get('id'),
                data:formData,
                cache:false,
                contentType: false,
                processData: false,
                
                    complete: function(response) 
                    {
                        $("#edit-submit").attr("disabled", false)

                        if(response.responseJSON.error == 0){

                            $('#edit-modal').click();

                            Swal.fire({
                                icon: 'success',
                                title: 'Success',
                                text: response.responseJSON.massage,
                            })
                            
                            $('#company').DataTable().ajax.reload()

                        }else{
                            Swal.fire({
                                icon: 'error',
                                title: 'Error',
                                text: response.responseJSON.massage,
                            })
                        }
                    }

                })
            }))

            $(".custom-file-input").on("change", function() {
                var fileName = $(this).val().split("\\").pop()
                $(this).siblings(".custom-file-label").addClass("selected").html(fileName)
            })
        })

        function getData(){
            $('#company').DataTable({
                processing: true,
                serverSide: true,
                ajax: '/admin/company/json',
                columns: [
                    { data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false },
                    { data: 'name', name: 'name' },
                    { data: 'email', name: 'email' },
                    { data: 'logo_url', name: 'logo_url' },
                    { data: 'website_url', name: 'website_url' },
                    { data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });
        }

        function deleteData(id){
            Swal.fire({
                title: 'Are You Sure?',
                showCancelButton: true,
                confirmButtonText: 'Delete',
                }).then((result) => {
                if(result.value == true) {
                    $.ajaxSetup({
                        headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    })

                    $.ajax({
                    type:'DELETE',
                    url: "/admin/company/"+id,
                    
                        complete: function(response) 
                        {
                            
                            if(response.responseJSON.error == 0){

                                Swal.fire({
                                    icon: 'success',
                                    title: 'Success',
                                    text: response.responseJSON.massage,
                                })
                                
                                $('#company').DataTable().ajax.reload()

                            }else{
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Error',
                                    text: response.responseJSON.massage,
                                })
                            }
                        }

                    })
                }
            })
        }

        function editData(id){
            $("#edit-content").html('')
            $("#edit-loader").show()
            $("#edit-submit").hide()

            $.ajaxSetup({
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            })

            $.ajax({
            type:'GET',
            url: "/admin/company/"+id+"/edit",
            
                complete: function(response) 
                {
                    $("#edit-content").html(response.responseText)
                    $("#edit-loader").hide()
                    $("#edit-submit").show()
                }

            })
        }
    </script>
@stop
