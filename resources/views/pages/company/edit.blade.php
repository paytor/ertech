@if(isset($data->logo))
<div class="row">
    <div class="col-sm-12">
        <center><img src="/storage/{{$data->logo}}" class="img-thumbnail" alt="{{$data->name}}" width="50%"></center>
    </div>
</div>
<br/><br/>
@endif
<input type="hidden" class="form-control" id="id" name="id" value="{{$data->id}}">
<input type="hidden" name="_method" value="PUT">
<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label for="name">Name:</label>
            <input type="text" class="form-control" id="name" name="name" value="{{$data->name}}">
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label for="email">Email:</label>
            <input type="email" class="form-control" id="email" value="{{$data->email}}" name="email">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label for="email">Logo:</label>
            <!-- <input type="file" name="logo" id="logo" accept="image/png, image/gif, image/jpeg" class="form-control"> -->
            <div class="custom-file mb-3">
                <input type="file" class="custom-file-input" id="logo" accept="image/png, image/gif, image/jpeg" name="logo">
                <label class="custom-file-label" for="logo">Choose file</label>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label for="website">Website:</label>
            <input type="text" class="form-control" value="{{$data->website}}" id="website" name="website">
        </div>
    </div>
</div>