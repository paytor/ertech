@if(isset($data->logo))
<div class="row">
    <div class="col-sm-12">
        <center><img src="/storage/{{$data->logo}}" class="img-thumbnail" alt="{{$data->name}}" width="50%"></center>
    </div>
</div>
<br/><br/>
@endif
<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <p>Name : <b>{{$data->name}}</b></p>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <p>Email : <b>{{$data->email}}</b></p>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <p>Website : <b>{{$data->website}}</b></p>
        </div>
    </div>
</div>