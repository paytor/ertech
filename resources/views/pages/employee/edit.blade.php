<input type="hidden" class="form-control" id="id" name="id" value="{{$data->id}}">
<input type="hidden" name="_method" value="PUT">
<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label for="first_name">First Name:</label>
            <input type="text" class="form-control" id="first_name" value="{{$data->first_name}}" name="first_name">
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label for="last_name">Last Name:</label>
            <input type="text" class="form-control" id="last_name" value="{{$data->last_name}}" name="last_name">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label for="email">Email:</label>
            <input type="email" class="form-control" id="email" value="{{$data->email}}" name="email">
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label for="phone">Phone:</label>
            <input type="text" class="form-control" pattern="[0-9]+" value="{{$data->phone}}" id="phone" name="phone">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label for="company">Company:</label>
            <select class="select-company" style="width:100%!important;" name="company" id="company">
                @foreach($company as $val)
                <option value="{{$val->id}}" <?php if($data->company == $val->id){echo 'selected';} ?>>{{$val->name}}</option>
                @endforeach
            </select>
        </div>
    </div>
</div>