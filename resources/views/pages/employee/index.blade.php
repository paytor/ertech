@extends('adminlte::page')

@section('title', 'GR Tech')

@section('css')
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <style>
        .select2-container .select2-selection--single {
            height: 100%;
        }
    </style>
@stop

@section('content_header')
    <h1 class="m-0 text-dark">Employee</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#create-modal">
                        <i class='fas fa-plus-circle'></i> Create
                    </button>
                    <br/><br/>
                    <div class="table-responsive">
                    <table class="table table-bordered" id="employee" width="100%">
                        <thead>
                            <tr>
                                <th>Index</th>
                                <th>Full Name</th>
                                <th>Company</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- The Modal -->
    <div class="modal fade" id="create-modal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
            <form enctype="multipart/form-data" id="create-form">
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Create</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                
                <!-- Modal body -->
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="first_name">First Name:</label>
                                <input type="text" class="form-control" id="first_name" name="first_name">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="last_name">Last Name:</label>
                                <input type="text" class="form-control" id="last_name" name="last_name">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="email">Email:</label>
                                <input type="email" class="form-control" id="email" name="email">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="phone">Phone:</label>
                                <input type="text" class="form-control" pattern="[0-9]+" id="phone" name="phone">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="company">Company:</label>
                                <select class="select-company" style="width:100%!important;" name="company" id="company">
                                    @foreach($company as $val)
                                    <option value="{{$val->id}}">{{$val->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                
                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="submit" id="create-submit" class="btn btn-success mr-1"><i class='fas fa-plus-circle'></i> Submit</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class='far fa-window-close'></i> Close</button>
                </div>
                
            </form>
            </div>
        </div>
    </div>

    <!-- The Edit Modal -->
    <div class="modal fade" id="edit-modal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
            <form enctype="multipart/form-data" id="edit-form">
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Edit</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                
                <!-- Modal body -->
                <div class="modal-body">
                    <div id="edit-loader">
                        <center><i class="fa fa-spinner fa-spin" style="font-size:24px"></i></center>
                    </div>
                    <div id="edit-content"></div>
                </div>
                
                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="submit" style="display:none;" id="edit-submit" class="btn btn-info mr-1"><i class='far fa-edit'></i> Edit</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class='far fa-window-close'></i> Close</button>
                </div>
                
            </form>
            </div>
        </div>
    </div>

    <!-- The Company Modal -->
    <div class="modal fade" id="company-modal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Detail</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                
                <!-- Modal body -->
                <div class="modal-body">
                    <div id="company-loader">
                        <center><i class="fa fa-spinner fa-spin" style="font-size:24px"></i></center>
                    </div>
                    <div id="company-content"></div>
                </div>
                
                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class='far fa-window-close'></i> Close</button>
                </div>
                
            </div>
        </div>
    </div>
@stop


@section('js')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script>
        $( document ).ready(function() {
            $('.select-company').select2();

            $('#create-form').on('submit',(function(e) {
                $.ajaxSetup({
                    headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                })

                $("#create-submit").attr("disabled", true)

                e.preventDefault();
                var formData = new FormData(this);

                $.ajax({
                type:'POST',
                url: "/admin/employee",
                data:formData,
                cache:false,
                contentType: false,
                processData: false,
                
                    complete: function(response) 
                    {
                        $("#create-submit").attr("disabled", false)

                        if(response.responseJSON.error == 0){

                            $("#first_name").val('')
                            $("#last_name").val('')
                            $("#email").val('')
                            $("#phone").val('')
                            $("#company").val('')

                            $('#create-modal').click();

                            Swal.fire({
                                icon: 'success',
                                title: 'Success',
                                text: response.responseJSON.massage,
                            })
                            
                            $('#employee').DataTable().ajax.reload()

                        }else{
                            Swal.fire({
                                icon: 'error',
                                title: 'Error',
                                text: response.responseJSON.massage,
                            })
                        }
                    }

                })
            }))

            $('#edit-form').on('submit',(function(e) {
                $.ajaxSetup({
                    headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                })

                $("#edit-submit").attr("disabled", true)

                e.preventDefault()
                var formData = new FormData(this)

                $.ajax({
                type:'POST',
                url: "/admin/employee/"+formData.get('id'),
                data:formData,
                cache:false,
                contentType: false,
                processData: false,
                
                    complete: function(response) 
                    {
                        $("#edit-submit").attr("disabled", false)

                        if(response.responseJSON.error == 0){

                            $('#edit-modal').click();

                            Swal.fire({
                                icon: 'success',
                                title: 'Success',
                                text: response.responseJSON.massage,
                            })
                            
                            $('#employee').DataTable().ajax.reload()

                        }else{
                            Swal.fire({
                                icon: 'error',
                                title: 'Error',
                                text: response.responseJSON.massage,
                            })
                        }
                    }

                })
            }))
        })

        $('#employee').DataTable({
            processing: true,
            serverSide: true,
            ajax: '/admin/employee/json',
            columns: [
                { data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false },
                { data: 'fullname', name: 'fullname' },
                { data: 'company_detail', name: 'company_detail', orderable: false, searchable: false },
                { data: 'email', name: 'email' },
                { data: 'phone', name: 'phone' },
                { data: 'action', name: 'action', orderable: false, searchable: false},
            ]
        });

        function editData(id){
            $("#edit-content").html('')
            $("#edit-loader").show()
            $("#edit-submit").hide()

            $.ajaxSetup({
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            })

            $.ajax({
            type:'GET',
            url: "/admin/employee/"+id+"/edit",
            
                complete: function(response) 
                {
                    $("#edit-content").html(response.responseText)
                    $("#edit-loader").hide()
                    $("#edit-submit").show()
                }

            })
        }

        function deleteData(id){
            Swal.fire({
                title: 'Are You Sure?',
                showCancelButton: true,
                confirmButtonText: 'Delete',
                }).then((result) => {
                if(result.value == true) {
                    $.ajaxSetup({
                        headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    })

                    $.ajax({
                    type:'DELETE',
                    url: "/admin/employee/"+id,
                    
                        complete: function(response) 
                        {
                            
                            if(response.responseJSON.error == 0){

                                Swal.fire({
                                    icon: 'success',
                                    title: 'Success',
                                    text: response.responseJSON.massage,
                                })
                                
                                $('#employee').DataTable().ajax.reload()

                            }else{
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Error',
                                    text: response.responseJSON.massage,
                                })
                            }
                        }

                    })
                }
            })
        }

        function companyDetail(id){
            $("#company-content").html('')
            $("#company-loader").show()

            $.ajaxSetup({
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            })

            $.ajax({
            type:'GET',
            url: "/admin/company/"+id,
            
                complete: function(response) 
                {
                    $("#company-content").html(response.responseText)
                    $("#company-loader").hide()
                }

            })
        }
    </script>
@stop
